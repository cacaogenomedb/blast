.POSIX:

all:
	docker build -t registry.gitlab.com/cacaogenomedb/blast:1.1-assembly -f build/1.1-assembly/Dockerfile .
	docker build -t registry.gitlab.com/cacaogenomedb/blast:1.1-annotation -f build/1.1-annotation/Dockerfile .
